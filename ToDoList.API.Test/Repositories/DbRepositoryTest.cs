using Dapper;
using Moq;
using System;
using Xunit;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ToDoList.API.Controllers;
using ToDoList.API.Repositories;
using ToDoList.API.Services;
using ToDoList.Core.Entities;

namespace ToDoList.API.Test.Repositories
{
    public class DbRepositoryTest
    {
        private readonly Mock<ILogger<DbRepository>> _logger;
        private readonly Mock<ICustomConfiguration> _config;
        private readonly Mock<IDbConnectionFactory> _connFactory;
        private readonly Mock<ICustomDapper> _dapper;
        private readonly IDbRepository _repo;
        private readonly TodoItem _mockObject;
        public DbRepositoryTest()
        {
            _logger = new Mock<ILogger<DbRepository>>();
            _config = new Mock<ICustomConfiguration>();
            _connFactory = new Mock<IDbConnectionFactory>();
            _dapper = new Mock<ICustomDapper>();
            _repo = new DbRepository(_config.Object, _logger.Object, _connFactory.Object, _dapper.Object);
            _connFactory.Setup(_ => _.GetConnection(It.IsAny<string>())).Returns(It.IsAny<IDbConnection>());
            _mockObject = new TodoItem 
            {
                Id = 0
            };
        }

        [Fact]
        public async void QueryGetList_WhenCalled_ReturnsValue()
        {
            // Arrange
            var expected = new List<TodoItem> 
            {
                _mockObject
            };
            _dapper.Setup(_ => _.QueryAsync<TodoItem>(It.IsAny<IDbConnection>(), It.IsAny<string>(), null)).ReturnsAsync(expected);
            // Act
            var actual = await _repo.QueryGetList<TodoItem>(It.IsAny<string>());
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void QueryGetList_WhenCalled_ThrowsException()
        {
            // Arrange
            _dapper.Setup(_ => _.QueryAsync<TodoItem>(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            await Assert.ThrowsAsync<Exception>(() => _repo.QueryGetList<TodoItem>(It.IsAny<string>()));
        }

        [Fact]
        public async void QueryGetItem_WhenCalled_ReturnsValue()
        {
            // Arrange
            var expected = _mockObject;
            _dapper.Setup(_ => _.QueryFirstOrDefaultAsync<TodoItem>(It.IsAny<IDbConnection>(), It.IsAny<string>(), null)).ReturnsAsync(expected);
            // Act
            var actual = await _repo.QueryGetItem<TodoItem>(It.IsAny<string>());
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void QueryGetItem_WhenCalled_ThrowsException()
        {
            // Arrange
            _dapper.Setup(_ => _.QueryFirstOrDefaultAsync<TodoItem>(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            await Assert.ThrowsAsync<Exception>(() => _repo.QueryGetItem<TodoItem>(It.IsAny<string>()));
        }

        [Fact]
        public async void Execute_WhenCalled_ReturnsValue()
        {
            // Arrange
            var expected = 1;
            _dapper.Setup(_ => _.ExecuteAsync(It.IsAny<IDbConnection>(), It.IsAny<string>(), null)).ReturnsAsync(expected);
            // Act
            var actual = await _repo.Execute(It.IsAny<string>());
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void Execute_WhenCalled_ThrowsException()
        {
            // Arrange
            _dapper.Setup(_ => _.ExecuteAsync(It.IsAny<IDbConnection>(), It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            await Assert.ThrowsAsync<Exception>(() => _repo.Execute(It.IsAny<string>()));
        }
    }
}