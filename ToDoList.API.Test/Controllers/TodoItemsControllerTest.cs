using Moq;
using System;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net;
using ToDoList.Core.Entities;
using ToDoList.API.Controllers;
using ToDoList.API.Repositories;
using ToDoList.API.Services;

namespace ToDoList.API.Test.Controllers
{
    public class TodoItemsControllerTest
    {
        private readonly Mock<ITodoItemService> _service;
        private readonly TodoItemsController _controller;

        public TodoItemsControllerTest()
        {
            _service = new Mock<ITodoItemService>();
            _controller = new TodoItemsController(_service.Object);
        }

        [Fact]
        public async void GetItems_WhenCalled_ReturnsOkResult()
        {
            // Arrange
            // Act
            var response = await _controller.GetItems();
            // Assert
            Assert.IsType<OkObjectResult>(response.Result);
        }

        [Fact]
        public async void GetItems_Exception_WhenCalled_ReturnsInternalServerErrorResult()
        {
            // Arrange
            _service.Setup(_ => _.GetTodoItems()).ThrowsAsync(new Exception());
            // Act
            var response = await _controller.GetItems();
            // Assert
            var objectResult = Assert.IsType<ObjectResult>(response.Result);
            Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
        }

        [Fact]
        public async void GetItems_WhenCalled_ReturnsAllItems()
        {
            // Arrange
            var expected = new List<TodoItem>
            {
                new TodoItem
                {
                    Id = 0
                }
            };
            _service.Setup(_ => _.GetTodoItems()).ReturnsAsync(expected);
            // Act
            var response = await _controller.GetItems();
            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var actual = Assert.IsType<List<TodoItem>>(objectResult.Value);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void GetItem_WhenCalled_ReturnsOkResult()
        {
            // Arrange
            _service.Setup(_ => _.GetTodoItem(It.IsAny<int>())).ReturnsAsync(new TodoItem());
            // Act
            var response = await _controller.GetItem(It.IsAny<int>());
            // Assert
            Assert.IsType<OkObjectResult>(response.Result);
        }

        [Fact]
        public async void GetItem_InvalidId_WhenCalled_ReturnsBadRequestResult()
        {
            // Arrange
            _service.Setup(_ => _.GetTodoItem(It.IsAny<int>())).ReturnsAsync(default(TodoItem));
            // Act
            var response = await _controller.GetItem(It.IsAny<int>());
            // Assert
            Assert.IsType<BadRequestResult>(response.Result);
        }

        [Fact]
        public async void GetItem_WhenCalled_ReturnsItem()
        {
            // Arrange
            var itemId = 1;
            var expected = new TodoItem
            {
                Id = itemId
            };
            _service.Setup(_ => _.GetTodoItem(itemId)).ReturnsAsync(expected);
            // Act
            var response = await _controller.GetItem(itemId);
            // Assert
            var objectResult = Assert.IsType<OkObjectResult>(response.Result);
            var actual = Assert.IsType<TodoItem>(objectResult.Value);
            Assert.Equal(expected, actual);
        }

        [Fact]
        public async void GetItem_Exception_WhenCalled_ReturnsInternalServerErrorResult()
        {
            // Arrange
            _service.Setup(_ => _.GetTodoItem(It.IsAny<int>())).ThrowsAsync(new Exception());
            // Act
            var response = await _controller.GetItem(It.IsAny<int>());
            // Assert
            var objectResult = Assert.IsType<ObjectResult>(response.Result);
            Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
        }

        [Fact]
        public async void PutItem_WhenCalled_ReturnsOkResult()
        {
            // Arrange
            var itemId = 1;
            var item = new TodoItem
            {
                Id = itemId
            };
            _service.Setup(_ => _.UpdateTodoItem(item)).ReturnsAsync(true);
            // Act
            var response = await _controller.PutItem(itemId, item);
            // Assert
            Assert.IsType<OkResult>(response);
        }

        [Fact]
        public async void PutItem_InvalidId_WhenCalled_ReturnsBadRequestResult()
        {
            // Arrange
            var itemId = -1;
            var item = new TodoItem
            {
                Id = 1
            };
            // Act
            var response = await _controller.PutItem(itemId, item);
            // Assert
            Assert.IsType<BadRequestResult>(response);
        }

        [Fact]
        public async void PutItem_NotUpdated_WhenCalled_ReturnsUnprocessableEntityResult()
        {
            // Arrange
            var itemId = -1;
            var item = new TodoItem
            {
                Id = itemId
            };
            _service.Setup(_ => _.UpdateTodoItem(item)).ReturnsAsync(false);
            // Act
            var response = await _controller.PutItem(itemId, item);
            // Assert
            Assert.IsType<UnprocessableEntityResult>(response);
        }

        [Fact]
        public async void PutItem_Exception_WhenCalled_ReturnsInternalServerErrorResult()
        {
            // Arrange
            var itemId = 1;
            var item = new TodoItem
            {
                Id = itemId
            };
            _service.Setup(_ => _.UpdateTodoItem(item)).ThrowsAsync(new Exception());
            // Act
            var response = await _controller.PutItem(itemId, item);
            // Assert
            var objectResult = Assert.IsType<ObjectResult>(response);
            Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
        }

        [Fact]
        public async void PostItem_WhenCalled_ReturnsOkResult()
        {
            // Arrange
            _service.Setup(_ => _.CreateTodoItem(It.IsAny<TodoItem>())).ReturnsAsync(true);
            // Act
            var response = await _controller.PostItem(It.IsAny<TodoItem>());
            // Assert
            Assert.IsType<OkResult>(response);
        }

        [Fact]
        public async void PostItem_NotCreated_WhenCalled_ReturnsUnprocessableEntityResult()
        {
            // Arrange
            _service.Setup(_ => _.CreateTodoItem(It.IsAny<TodoItem>())).ReturnsAsync(false);
            // Act
            var response = await _controller.PostItem(It.IsAny<TodoItem>());
            // Assert
            Assert.IsType<UnprocessableEntityResult>(response);
        }

        [Fact]
        public async void PostItem_Exception_WhenCalled_ReturnsInternalServerErrorResult()
        {
            // Arrange
            _service.Setup(_ => _.CreateTodoItem(It.IsAny<TodoItem>())).ThrowsAsync(new Exception());
            // Act
            var response = await _controller.PostItem(It.IsAny<TodoItem>());
            // Assert
            var objectResult = Assert.IsType<ObjectResult>(response);
            Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
        }

        [Fact]
        public async void DeleteItem_WhenCalled_ReturnsOkResult()
        {
            // Arrange
            _service.Setup(_ => _.DeleteTodoItem(It.IsAny<int>())).ReturnsAsync(true);
            // Act
            var response = await _controller.DeleteItem(It.IsAny<int>());
            // Assert
            Assert.IsType<OkResult>(response);
        }

        [Fact]
        public async void DeleteItem_NotDeleted_WhenCalled_ReturnsUnprocessableEntityResult()
        {
            // Arrange
            _service.Setup(_ => _.DeleteTodoItem(It.IsAny<int>())).ReturnsAsync(false);
            // Act
            var response = await _controller.DeleteItem(It.IsAny<int>());
            // Assert
            Assert.IsType<BadRequestResult>(response);
        }

        [Fact]
        public async void DeleteItem_Exception_WhenCalled_ReturnsInternalServerErrorResult()
        {
            // Arrange
            _service.Setup(_ => _.DeleteTodoItem(It.IsAny<int>())).ThrowsAsync(new Exception());
            // Act
            var response = await _controller.DeleteItem(It.IsAny<int>());
            // Assert
            var objectResult = Assert.IsType<ObjectResult>(response);
            Assert.Equal((int)HttpStatusCode.InternalServerError, objectResult.StatusCode);
        }
    }
}
