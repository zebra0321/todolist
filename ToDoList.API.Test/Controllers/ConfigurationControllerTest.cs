using Moq;
using System;
using Xunit;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.API.Controllers;
using ToDoList.API.Repositories;

namespace ToDoList.API.Test.Controllers
{
    public class ConfigurationControllerTest
    {
        private readonly Mock<ILogger<ConfigurationController>> _logger;
        private readonly Mock<ICustomConfiguration> _config;
        private readonly ConfigurationController _controller;

        public ConfigurationControllerTest()
        {
            _config = new Mock<ICustomConfiguration>();
            _logger = new Mock<ILogger<ConfigurationController>>();
            _controller = new ConfigurationController(_config.Object, _logger.Object);
        }

        // [Fact]
        // public void Test_WhenCalled_ReturnsValue()
        // {
        //     // Arrange
        //     var expected = "test";
        //     // Act
        //     var response = _controller.Test();
        //     // Assert
        //     Assert.Equal(expected, response);
        // }

        [Fact]
        public void GetValue_WhenCalled_ReturnsValue()
        {
            // Arrange
            var key = "test";
            var expected = "test";
            _config.Setup(_ => _.GetValue<string>(key)).Returns(expected);
            // Act
            var actual = _controller.GetValue(key);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetValue_Exception_WhenCalled_ReturnsErrorMessage()
        {
            // Arrange
            var expected = "testing";
            _config.Setup(_ => _.GetValue<string>(It.IsAny<string>())).Throws(new Exception(expected));
            // Act
            var actual = _controller.GetValue(It.IsAny<string>());
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}

