using Moq;
using System;
using Xunit;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.API.Controllers;
using ToDoList.API.Repositories;
using ToDoList.API.Services;
using ToDoList.Core.Entities;

namespace ToDoList.API.Test.Services
{
    public class TodoItemServiceTest
    {
        private readonly Mock<ILogger<TodoItemService>> _logger;
        private readonly Mock<IDbRepository> _repo;
        private readonly TodoItemService _service;

        public TodoItemServiceTest()
        {
            _repo = new Mock<IDbRepository>();
            _logger = new Mock<ILogger<TodoItemService>>();
            _service = new TodoItemService(_logger.Object, _repo.Object);
        }

        #region Mock data
        public static IEnumerable<object[]> UpdateTodoItem_Updated_WhenCalled_ReturnsTrue_MockData()
        => new object[][]
        {
            new object[] 
            {
                new TodoItem
                {
                    Id = 0
                },
            },
            new object[]
            {
                new TodoItem
                {
                    Id = 0,
                    Title = "test"
                },
            },
            new object[]
            {
                new TodoItem
                {
                    Id = 0,
                    DueDate = DateTime.Now
                },
            },
            new object[]
            {
                new TodoItem
                {
                    Id = 0,
                    IsComplete = false
                },
            }
        };
        #endregion

        [Fact]
        public async void GetTodoItems_WhenCalled_ReturnsAllItems()
        {
            // Arrange
            var expected = new List<TodoItem> 
            {
                new TodoItem()
            };
            _repo.Setup(_ => _.QueryGetList<TodoItem>(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(expected);
            // Act
            var response = await _service.GetTodoItems();
            // Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void GetTodoItems_WhenCalled_ThrowsException()
        {
            // Arrange
            _repo.Setup(_ => _.QueryGetList<TodoItem>(It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _service.GetTodoItems());
        }

        [Fact]
        public async void GetTodoItem_WhenCalled_ReturnsItem()
        {
            // Arrange
            var expected = new TodoItem();
            _repo.Setup(_ => _.QueryGetItem<TodoItem>(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(expected);
            // Act
            var response = await _service.GetTodoItem(It.IsAny<int>());
            // Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void GetTodoItem_WhenCalled_ThrowsException()
        {
            // Arrange
            _repo.Setup(_ => _.QueryGetItem<TodoItem>(It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _service.GetTodoItem(It.IsAny<int>()));
        }

        [Theory]
        [MemberData(nameof(UpdateTodoItem_Updated_WhenCalled_ReturnsTrue_MockData))]
        public async void UpdateTodoItem_Updated_WhenCalled_ReturnsTrue(TodoItem item)
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(1);
            // Act
            var actual = await _service.UpdateTodoItem(item);
            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void UpdateTodoItem_UpdateFailed_WhenCalled_ReturnsFalse()
        {
            // Arrange
            var item = new TodoItem();
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(0);
            // Act
            var actual = await _service.UpdateTodoItem(item);
            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void UpdateTodoItem_WhenCalled_ThrowsException()
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _service.UpdateTodoItem(It.IsAny<TodoItem>()));
        }

        [Fact]
        public async void CreateTodoItem_Created_WhenCalled_ReturnsTrue()
        {
            // Arrange
            var item = new TodoItem();
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(1);
            // Act
            var actual = await _service.CreateTodoItem(item);
            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void CreateTodoItem_CreateFailed_WhenCalled_ReturnsFalse()
        {
            // Arrange
            var item = new TodoItem();
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(0);
            // Act
            var actual = await _service.CreateTodoItem(item);
            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void CreateTodoItem_WhenCalled_ThrowsException()
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _service.CreateTodoItem(It.IsAny<TodoItem>()));
        }

        [Fact]
        public async void DeleteTodoItem_Deleted_WhenCalled_ReturnsTrue()
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(1);
            // Act
            var actual = await _service.DeleteTodoItem(It.IsAny<int>());
            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void DeleteTodoItem_DeleteFailed_WhenCalled_ReturnsFalse()
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ReturnsAsync(0);
            // Act
            var actual = await _service.DeleteTodoItem(It.IsAny<int>());
            // Assert
            Assert.False(actual);
        }

        [Fact]
        public void DeleteTodoItem_WhenCalled_ThrowsException()
        {
            // Arrange
            _repo.Setup(_ => _.Execute(It.IsAny<string>(), It.IsAny<object>())).ThrowsAsync(new Exception());
            // Act
            // Assert
            Assert.ThrowsAsync<Exception>(() => _service.DeleteTodoItem(It.IsAny<int>()));
        }
    }
}