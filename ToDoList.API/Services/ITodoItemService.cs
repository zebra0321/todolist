using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoList.Core.Entities;

namespace ToDoList.API.Services
{
    public interface ITodoItemService 
    {
        public Task<IEnumerable<TodoItem>> GetTodoItems();
        public Task<TodoItem> GetTodoItem(int id);
        public Task<bool> UpdateTodoItem(TodoItem item);
        public Task<bool> CreateTodoItem(TodoItem item);
        public Task<bool> DeleteTodoItem(int id);
    }
}