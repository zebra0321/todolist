using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ToDoList.API.Repositories;
using ToDoList.Core.Entities;

namespace ToDoList.API.Services
{
    public class TodoItemService: ITodoItemService 
    {
        private readonly ILogger<TodoItemService> _logger;
        private readonly IDbRepository _repository; 

        public TodoItemService(ILogger<TodoItemService> logger, IDbRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        public async Task<IEnumerable<TodoItem>> GetTodoItems() 
        {
            try 
            {
                string query = "SELECT * FROM todoitems ORDER BY due_date DESC";
                return await _repository.QueryGetList<TodoItem>(query);
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex, nameof(GetTodoItems));
                throw;
            }
        }

        public async Task<TodoItem> GetTodoItem(int id)
        {
            try 
            {
                string query = "SELECT * FROM todoitems WHERE id = @id";
                object parameters = new { id = id };
                return await _repository.QueryGetItem<TodoItem>(query, parameters);
            }
            catch (Exception ex) 
            {
                _logger.LogError(ex, nameof(GetTodoItem));
                throw;
            }
        }

        public async Task<bool> UpdateTodoItem(TodoItem item)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                string query = "UPDATE todoitems SET ";

                if (!string.IsNullOrEmpty(item.Title)) 
                {
                    query += "title = @title,"; 
                    parameters.Add("title", item.Title);
                }

                if (item.DueDate.HasValue)
                {
                    query += "due_date = @dueDate,";
                    parameters.Add("dueDate", item.DueDate);
                }

                if (item.IsComplete.HasValue)
                {
                    query += "is_complete = @isComplete,";
                    parameters.Add("isComplete", item.IsComplete);
                }

                query += "modified_at = @modifiedAt,";
                parameters.Add("modifiedAt", DateTime.Now);

                query = query.TrimEnd(',');
                query += " WHERE id = @id";
                parameters.Add("id", item.Id);

                int updateCnt = await _repository.Execute(query, parameters);
                return updateCnt == 1;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(UpdateTodoItem));
                throw;
            }
        }

        public async Task<bool> CreateTodoItem(TodoItem item)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                string query = "INSERT INTO todoitems (title, due_date, is_complete, created_at) VALUES (@title, @dueDate, @isComplete, @createdAt)";
                
                parameters.Add("title", item.Title);
                parameters.Add("dueDate", item.DueDate);
                parameters.Add("isComplete", item.IsComplete);
                parameters.Add("createdAt", DateTime.Now);

                int createCnt = await _repository.Execute(query, parameters);
                return createCnt == 1;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(UpdateTodoItem));
                throw;
            }
        }

        public async Task<bool> DeleteTodoItem(int id)
        {
            try
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                string query = "DELETE FROM todoitems WHERE id = @id;";
                parameters.Add("id", id);

                int createCnt = await _repository.Execute(query, parameters);
                return createCnt == 1;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(UpdateTodoItem));
                throw;
            }
        }
    }
}