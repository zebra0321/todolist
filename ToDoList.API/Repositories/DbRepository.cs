using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ToDoList.API.Constants;

namespace ToDoList.API.Repositories 
{
    public class DbRepository: IDbRepository 
    {
        private readonly ICustomConfiguration _config;
        private readonly ILogger<DbRepository> _logger;
        private readonly IDbConnectionFactory _connFactory;
        private readonly ICustomDapper _dapper;
        private readonly string _dbConnectionString;

        public DbRepository(ICustomConfiguration config, ILogger<DbRepository> logger, IDbConnectionFactory connFactory, ICustomDapper dapper)
        {
            _config = config;
            _logger = logger;
            _connFactory = connFactory;
            _dapper = dapper;
            _dbConnectionString = _config.GetConnectionString(ConfigurationConstants.dbConnectionStringKey);
        }

        public async Task<IEnumerable<T>> QueryGetList<T>(string query, object parameters = null)
        {
            try 
            {
                using (IDbConnection db = _connFactory.GetConnection(_dbConnectionString))
                {
                    return await _dapper.QueryAsync<T>(db, query, parameters);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(QueryGetList));
                throw;
            }
        }

        public async Task<T> QueryGetItem<T>(string query, object parameters = null)
        {
            try
            {
                using (IDbConnection db = _connFactory.GetConnection(_dbConnectionString))
                {
                    return await _dapper.QueryFirstOrDefaultAsync<T>(db, query, parameters);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(QueryGetItem));
                throw;
            }
        }

        public async Task<int> Execute(string query, object parameters = null)
        {
            try
            {
                using (IDbConnection db = _connFactory.GetConnection(_dbConnectionString))
                {
                    return await _dapper.ExecuteAsync(db, query, parameters);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, nameof(QueryGetItem));
                throw;
            }
        }
    }
}