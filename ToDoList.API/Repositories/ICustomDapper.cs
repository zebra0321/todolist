using Dapper;
using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoList.API.Repositories
{
    public interface ICustomDapper
    {
        public Task<IEnumerable<T>> QueryAsync<T>(IDbConnection conn, string query, object parameters = null);
        public Task<T> QueryFirstOrDefaultAsync<T>(IDbConnection conn, string query, object parameters = null);
        public Task<int> ExecuteAsync(IDbConnection conn, string query, object parameters = null);
    }
}