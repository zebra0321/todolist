using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace ToDoList.API.Repositories
{
    [ExcludeFromCodeCoverage]
    public class DbConnectionFactory: IDbConnectionFactory
    {
        public DbConnectionFactory()
        {

        }

        public IDbConnection GetConnection(string connStr)
        {
            return new SqlConnection(connStr);
        }
    }
}