using Dapper;
using System.Data;
using System.Diagnostics.CodeAnalysis;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoList.API.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomDapper: ICustomDapper
    {
        public async Task<IEnumerable<T>> QueryAsync<T>(IDbConnection conn, string query, object parameters = null)
        {
            return await conn.QueryAsync<T>(query, parameters);
        }

        public async Task<T> QueryFirstOrDefaultAsync<T>(IDbConnection conn, string query, object parameters = null)
        {
            return await conn.QueryFirstOrDefaultAsync<T>(query, parameters);
        }

        public async Task<int> ExecuteAsync(IDbConnection conn, string query, object parameters = null)
        {
            return await conn.ExecuteAsync(query, parameters);
        }
    }
}