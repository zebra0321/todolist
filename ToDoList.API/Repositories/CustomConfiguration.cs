using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace ToDoList.API.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomConfiguration: ICustomConfiguration
    {
        private readonly ILogger<CustomConfiguration> _logger;
        private readonly IConfiguration _config;
        public CustomConfiguration(ILogger<CustomConfiguration> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        public T GetValue<T>(string key)
        {
            T result = default(T);

            try
            {
                result = _config.GetValue<T>(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CustomConfiguration > GetValue");
            }

            return result;
        }

        public string GetConnectionString(string key)
        {
            string result = "";

            try
            {
                result = _config.GetConnectionString(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "CustomConfiguration > GetConnectionString");
            }

            return result;
        }
    }
}