using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoList.API.Repositories 
{
    public interface IDbRepository 
    {
        public Task<IEnumerable<T>> QueryGetList<T>(string query, object parameters = null);
        public Task<T> QueryGetItem<T>(string query, object parameters = null);
        public Task<int> Execute(string query, object parameters = null);
    }
}