using System;

namespace ToDoList.API.Repositories
{
    public interface ICustomConfiguration
    {
        public T GetValue<T>(string key);

        public string GetConnectionString(string key);
    }
}