using System.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ToDoList.API.Repositories
{
    public interface IDbConnectionFactory
    {
        public IDbConnection GetConnection(string connStr);
    }
}