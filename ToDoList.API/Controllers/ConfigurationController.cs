using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.API.Services;
using ToDoList.API.Repositories;

namespace ToDoList.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ConfigurationController: ControllerBase
    {
        private readonly ICustomConfiguration _config;
        private readonly ILogger<ConfigurationController> _logger;

        public ConfigurationController(ICustomConfiguration config, ILogger<ConfigurationController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [HttpGet]
        [ExcludeFromCodeCoverage]
        public string Test() => "test";
        
        [HttpGet("{key}")]
        public string GetValue(string key)
        {
            try
            {
                return _config.GetValue<string>(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ConfigurationController > GetValue");
                return ex.Message;
            }
        }
    }
}