IF NOT EXISTS (SELECT 1 FROM sysobjects WHERE xtype = 'U' AND name = 'todoitems')
BEGIN
    CREATE TABLE todoitems (
        id INT IDENTITY(1, 1) PRIMARY KEY,
        title NVARCHAR(100) NOT NULL,
        due_date DATETIME NOT NULL,
        is_complete BIT NULL,
        created_at DATETIME NOT NULL,
        modified_at DATETIME NULL
    )
END
GO