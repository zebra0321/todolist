using System;
using System.Diagnostics.CodeAnalysis;

namespace ToDoList.API.Models
{
    [ExcludeFromCodeCoverage]
    public class TodoItem
    {
        public int Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime DueDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
    }

}