using Dapper.FluentMap.Mapping;
using ToDoList.Core.Entities;

namespace ToDoList.Core.Maps
{
    public class TodoItemMap: EntityMap<TodoItem>
    {
        public TodoItemMap()
        {
            Map(i => i.Id).ToColumn("id");
            Map(i => i.Title).ToColumn("title");
            Map(i => i.DueDate).ToColumn("due_date");
            Map(i => i.IsComplete).ToColumn("is_complete");
            Map(i => i.CreatedAt).ToColumn("created_at");
            Map(i => i.ModifiedAt).ToColumn("modified_at");
        }
    }
}