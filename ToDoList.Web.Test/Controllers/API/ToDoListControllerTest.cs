using Moq;
using Xunit;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Services;
using ToDoList.Web.Controllers.API;
using ToDoList.Web.Models;

namespace ToDoList.Web.Test.Controllers.API
{
    public class ToDoListControllerTest
    {
        private readonly Mock<ILogger<ToDoListController>> _logger;
        private readonly Mock<IApiService> _apiService;
        private readonly ToDoListController _controller;

        public ToDoListControllerTest()
        {
            _logger = new Mock<ILogger<ToDoListController>>();
            _apiService = new Mock<IApiService>();
            _controller = new ToDoListController(_logger.Object, _apiService.Object);
        }

        [Fact]
        public async void Post_ModelStateValid()
        {
            // Arrange
            var item = new TodoItem 
            {
                Id = 0,
                IsComplete = false
            };

            _apiService.Setup(service => service.PutToDoItem(It.IsAny<TodoItem>())).ReturnsAsync(true);
            // Act
            var response = await _controller.Post(item);
            // Assert
            var objectResult = Assert.IsType<OkResult>(response);
        }

        [Fact]
        public async void Post_ModelStateInvalid_ReturnsBadRequest()
        {
            // Arrange
            _apiService.Setup(service => service.PutToDoItem(It.IsAny<TodoItem>())).ReturnsAsync(true);
            // Act
            var response = await _controller.Post(null);
            // Assert
            var objectResult = Assert.IsType<BadRequestResult>(response);
        }
    }
}

