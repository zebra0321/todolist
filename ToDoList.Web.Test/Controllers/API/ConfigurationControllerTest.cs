using Moq;
using Xunit;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Services;
using ToDoList.Web.Controllers.API;
using ToDoList.Web.Models;
using ToDoList.Web.Repositories;
using ToDoList.Web.Test.Mocks;

namespace ToDoList.Web.Test.Controllers.API
{
    public class ConfigurationControllerTest
    {
        private readonly Mock<ILogger<ConfigurationController>> _logger;
        private readonly ICustomConfiguration _config;
        private readonly ConfigurationController _controller;

        public ConfigurationControllerTest()
        {
            _config = new MockCustomConfiguration();
            _logger = new Mock<ILogger<ConfigurationController>>();
            _controller = new ConfigurationController(_config, _logger.Object);
        }

        [Fact]
        public void Test_ModelStateValid()
        {
            // Arrange
            var expected = "test";
            // Act
            var response = _controller.Test();
            // Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void GetValue_ModelStateValid()
        {
            // Arrange
            var key = "test";
            var expected = _config.GetValue<string>(key);
            // Act
            var response = _controller.GetValue(key);
            // Assert
            Assert.Equal(expected, response);
        }
    }
}

