using Moq;
using Xunit;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Services;
using ToDoList.Web.Controllers.API;
using ToDoList.Web.Models;
using ToDoList.Web.Repositories;
using ToDoList.Web.Test.Mocks;

namespace ToDoList.Web.Test.Controllers.API
{
    public class EnvironmentControllerTest
    {
        private readonly Mock<ILogger<EnvironmentController>> _logger;
        private readonly ICustomEnvironment _env;
        // private readonly Mock<ICustomEnvironment> _env;
        private readonly EnvironmentController _controller;

        public EnvironmentControllerTest()
        {
            _logger = new Mock<ILogger<EnvironmentController>>();
            _env = new MockCustomEnvironment();
            // _env = new Mock<ICustomEnvironment>();
            _controller = new EnvironmentController(_env, _logger.Object);
        }

        [Fact]
        public void Test_ModelStateValid()
        {
            // Arrange
            var expected = "test";
            // Act
            var response = _controller.Test();
            // Assert
            Assert.Equal(expected, response);
        }

        [Fact]
        public void GetValue_ModelStateValid()
        {
            // Arrange
            var key = "test";
            var expected = _env.GetEnvironmentVariable(key);
            // Act
            var response = _controller.GetValue(key);
            // Assert
            Assert.Equal(expected, response);
        }
    }
}

