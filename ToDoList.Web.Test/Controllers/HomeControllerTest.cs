using Moq;
using Xunit;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using ToDoList.Web.Services;
using ToDoList.Web.Controllers;
using ToDoList.Web.Models;
using ToDoList.Web.ViewModels;

namespace ToDoList.Web.Test.Controllers
{
    public class HomeControllerTest
    // : IDisposable
    {
        private readonly Mock<ILogger<HomeController>> _logger;
        private readonly Mock<IApiService> _apiService;
        private readonly HomeController _controller;
        // private readonly Activity _activity; 

        public HomeControllerTest()
        {
            _logger = new Mock<ILogger<HomeController>>();
            _apiService = new Mock<IApiService>();
            _controller = new HomeController(_logger.Object, _apiService.Object);
            // // For mocking System.Diagnostics.Activity
            // _activity = new Activity("HomeControllerTestActivity").Start();
        }

        [Fact]
        public async void Index_ActionExecutes_ReturnsViewResult()
        {
            // Arrange
            _apiService.Setup(service => service.GetToDoItems()).ReturnsAsync(GetTodoItemsMockData());
            // Act
            var result = await _controller.Index();
            // Assert
            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<HomeViewModel>(viewResult.ViewData.Model);
            Assert.Equal(1, model.listItems.Count);
        }

        private List<TodoItem> GetTodoItemsMockData() {
            return new List<TodoItem> {
                new TodoItem {
                    Id = 1,
                    Title = "Test1"
                }
            };
        }

        // [Fact]
        // public void Privacy_ActionExecutes_ReturnsViewResult(){
        //     var result = _controller.Privacy();
        //     Assert.IsType<ViewResult>(result);
        // }

        // [Fact]
        // public void Error_ActionExecutes_ReturnsViewResult(){
        //     var result = _controller.Error();
        //     Assert.IsType<ViewResult>(result);
        // }

        // public void Dispose() {
        //     _activity.Stop();
        // }
    }
}