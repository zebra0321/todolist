using Moq;
using Moq.Protected;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ToDoList.Web.Test.Extensions
{
    public static class MockHttpMessageHandlerExtension {
        public static void MockSendAsync(this Mock<HttpMessageHandler> mockHandler, HttpStatusCode statusCode, object body) {
            StringContent content = null;
            if (body != null) {
                content = new StringContent(JsonSerializer.Serialize(body));
            }

            mockHandler.Protected()
                       .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(), ItExpr.IsAny<CancellationToken>())
                       .ReturnsAsync(new HttpResponseMessage {
                           StatusCode = statusCode,
                           Content = content
                       });
        }
    }
}