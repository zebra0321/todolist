using Moq;
using Xunit;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using ToDoList.Web;
using ToDoList.Web.Services;
using ToDoList.Web.Repositories;
using ToDoList.Core.Entities;

namespace ToDoList.Web.Test.Services {
    public class ApiServiceTest {
        private readonly Mock<IApiRepository> _apiRepository;
        private readonly Mock<ILogger<ApiService>> _logger;
        private readonly IApiService _apiService;
        
        public ApiServiceTest() {
            _logger = new Mock<ILogger<ApiService>>();
            _apiRepository = new Mock<IApiRepository>();
            _apiRepository.Setup(repo => repo.GetToDoItems()).ReturnsAsync(new List<TodoItem>());
            _apiRepository.Setup(repo => repo.GetToDoItem(It.IsAny<int>())).ReturnsAsync(new TodoItem());
            _apiRepository.Setup(repo => repo.PutToDoItem(It.IsAny<Models.TodoItem>())).ReturnsAsync(true);
            _apiRepository.Setup(repo => repo.PostToDoItem(It.IsAny<Models.TodoItem>())).ReturnsAsync(true);
            _apiRepository.Setup(repo => repo.DeleteToDoItem(It.IsAny<int>())).ReturnsAsync(true);
            _apiService = new ApiService(_apiRepository.Object, _logger.Object);
        }

        [Fact]
        public async void GetToDoItems_ModelStateValid()
        {
            // Arrange
            // Act
            var actual = await _apiService.GetToDoItems();
            // Assert
            Assert.IsType<List<ToDoList.Web.Models.TodoItem>>(actual);
        }

        [Fact]
        public async void GetToDoItem_ModelStateValid()
        {
            // Arrange
            // Act
            var actual = await _apiService.GetToDoItem(It.IsAny<int>());
            // Assert
            Assert.IsType<ToDoList.Web.Models.TodoItem>(actual);
        }

        [Fact]
        public async void PutToDoItem_ModelStateValid()
        {
            // Arrange
            // Act
            var actual = await _apiService.PutToDoItem(It.IsAny<Models.TodoItem>());

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void PostToDoItem_ModelStateValid()
        {
            // Arrange
            // Act
            var actual = await _apiService.PostToDoItem(It.IsAny<Models.TodoItem>());

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void DeleteToDoItem_ModelStateValid()
        {
            // Arrange
            // Act
            var actual = await _apiService.DeleteToDoItem(It.IsAny<int>());

            // Assert
            Assert.True(actual);
        }
    }
}