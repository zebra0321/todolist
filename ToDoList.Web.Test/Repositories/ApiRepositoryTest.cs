using System;
using Xunit;
using Moq;
using Moq.Protected;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Constants;
using ToDoList.Web.Repositories;
using ToDoList.Web.Models;
using ToDoList.Web.Test.Extensions;
using ToDoList.Web.Test.Mocks;

namespace ToDoList.Web.Test.Services {
    public class ApiRepositoryTest {
        private Mock<ICustomHttpClientFactory> _customFactory;
        private readonly ICustomEnvironment _customEnvironment;
        private Mock<ICustomConfiguration> _configuration;
        private readonly Mock<ILogger<ApiRepository>> _logger;
        private IApiRepository _apiRepository;
        
        public ApiRepositoryTest() {
            _logger = new Mock<ILogger<ApiRepository>>();
            _configuration = new Mock<ICustomConfiguration>();
            _configuration.Setup(config => config.GetValue<string>(ConfigurationConstants.ApiBaseUrl)).Returns("https://apitest/");
        }

        [Fact]
        public async void GetToDoItems_ModelStateValid()
        {
            // Arrange
            var mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.MockSendAsync(HttpStatusCode.OK, new List<ToDoList.Core.Entities.TodoItem> {
                new ToDoList.Core.Entities.TodoItem {
                    Id = 0
                }
            });

            var mockClient = new HttpClient(mockHandler.Object);
            _customFactory = new Mock<ICustomHttpClientFactory>();
            _customFactory.Setup(factory => factory.CreateClient()).Returns(mockClient);
            _apiRepository = new ApiRepository(_customFactory.Object, _customEnvironment, _configuration.Object, _logger.Object);

            // Act
            var actual = await _apiRepository.GetToDoItems();

            // Assert
            Assert.IsType<List<ToDoList.Core.Entities.TodoItem>>(actual);
        }

        [Fact]
        public async void GetToDoItem_ModelStateValid()
        {
            // Arrange
            var mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.MockSendAsync(HttpStatusCode.OK, new ToDoList.Core.Entities.TodoItem {
                Id = 0
            });

            var mockClient = new HttpClient(mockHandler.Object);
            _customFactory = new Mock<ICustomHttpClientFactory>();
            _customFactory.Setup(factory => factory.CreateClient()).Returns(mockClient);
            _apiRepository = new ApiRepository(_customFactory.Object, _customEnvironment, _configuration.Object, _logger.Object);

            // Act
            var actual = await _apiRepository.GetToDoItem(It.IsAny<int>());
            // Assert
            Assert.IsType<ToDoList.Core.Entities.TodoItem>(actual);
        }

        [Fact]
        public async void PutToDoItem_ModelStateValid()
        {
            // Arrange
            TodoItem mockItem = new TodoItem {
                Id = 0,
                IsComplete = false
            };

            var mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.MockSendAsync(HttpStatusCode.OK, null);

            var mockClient = new HttpClient(mockHandler.Object);
            _customFactory = new Mock<ICustomHttpClientFactory>();
            _customFactory.Setup(factory => factory.CreateClient()).Returns(mockClient);
            _apiRepository = new ApiRepository(_customFactory.Object, _customEnvironment, _configuration.Object, _logger.Object);

            // Act
            var actual = await _apiRepository.PutToDoItem(mockItem);

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void PostToDoItem_ModelStateValid()
        {
            // Arrange
            TodoItem mockItem = new TodoItem {
                Id = 0,
                IsComplete = false
            };

            var mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.MockSendAsync(HttpStatusCode.OK, null);

            var mockClient = new HttpClient(mockHandler.Object);
            _customFactory = new Mock<ICustomHttpClientFactory>();
            _customFactory.Setup(factory => factory.CreateClient()).Returns(mockClient);
            _apiRepository = new ApiRepository(_customFactory.Object, _customEnvironment, _configuration.Object, _logger.Object);

            // Act
            var actual = await _apiRepository.PostToDoItem(mockItem);

            // Assert
            Assert.True(actual);
        }

        [Fact]
        public async void DeleteToDoItem_ModelStateValid()
        {
            // Arrange
            var dummyId = 0;

            var mockHandler = new Mock<HttpMessageHandler>();
            mockHandler.MockSendAsync(HttpStatusCode.OK, null);

            var mockClient = new HttpClient(mockHandler.Object);
            _customFactory = new Mock<ICustomHttpClientFactory>();
            _customFactory.Setup(factory => factory.CreateClient()).Returns(mockClient);
            _apiRepository = new ApiRepository(_customFactory.Object, _customEnvironment, _configuration.Object, _logger.Object);

            // Act
            var actual = await _apiRepository.DeleteToDoItem(dummyId);

            // Assert
            Assert.True(actual);
        }
    }
}