using System.Collections.Generic;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Test.Mocks
{
    public class MockCustomConfiguration: ICustomConfiguration
    {
        private readonly Dictionary<string, dynamic> _configDict;
        private readonly Dictionary<string, string> _connectionStrDict;

        public MockCustomConfiguration()
        {
            _configDict = new Dictionary<string, dynamic>() 
            {
                { "test", "test" }
            };

            _connectionStrDict = new Dictionary<string, string>()
            {
                { "test", "https://test" }
            };
        }

        public T GetValue<T>(string key)        
        {
            if (_configDict.TryGetValue(key, out var val))
            {
                return val;
            }
            else 
            {
                return default(T);
            }
        }

        public string GetConnectionString(string key)
        {
            if (_connectionStrDict.TryGetValue(key, out var val))
            {
                return val;
            }
            else 
            {
                return "";
            }
        }
    }
}