using System.Collections.Generic;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Test.Mocks
{
    public class MockCustomEnvironment: ICustomEnvironment
    {
        private readonly Dictionary<string, string> _envDict;

        public MockCustomEnvironment()
        {
            _envDict = new Dictionary<string, string>() 
            {
                { "test", "test" }
            };
        }

        public string GetEnvironmentVariable (string key)        
        {
            if (_envDict.TryGetValue(key, out var val))
            {
                return val;
            }
            else 
            {
                return "";
            }
        }
    }
}