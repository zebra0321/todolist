using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoList.Web.ViewModels.Components.ToDoList;

namespace ToDoList.Web.ViewComponents {
    public class ToDoListViewComponent: ViewComponent {
        public IViewComponentResult Invoke(IEnumerable<ToDoItemViewModel> items) {
            return View(items);
        }
    }
}