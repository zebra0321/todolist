﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Models;
using ToDoList.Web.Services;
using ToDoList.Web.ViewModels;
using ToDoList.Web.ViewModels.Components.ToDoList;

namespace ToDoList.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IApiService _apiService;

        public HomeController(ILogger<HomeController> logger, IApiService apiService)
        {
            _logger = logger;
            _apiService = apiService;
        }

        public async Task<IActionResult> Index()
        {
            try {
                var itemList = await _apiService.GetToDoItems();
                var viewModel = new HomeViewModel();
                if (itemList != null) {
                    viewModel.listItems = itemList.Select(item => new ToDoItemViewModel{
                        Id = item.Id,
                        DueDate = item.DueDate,
                        Title = item.Title,
                        IsComplete = item.IsComplete.HasValue ? item.IsComplete.Value : false
                    }).ToList();
                }

                return View(viewModel);
            }
            catch (Exception ex) {
                _logger.LogError(ex, "HomeController > Index");
                return View();
            }
        }

        // public IActionResult Privacy()
        // {
        //     return View();
        // }

        // [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        // public IActionResult Error()
        // {
        //     return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        // }
    }
}
