using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class EnvironmentController: ControllerBase
    {
        private readonly ICustomEnvironment _env;
        private readonly ILogger<EnvironmentController> _logger;

        public EnvironmentController(ICustomEnvironment env, ILogger<EnvironmentController> logger)
        {
            _env = env;
            _logger = logger;
        }

        [HttpGet]
        public string Test()
        {
            try
            {
                return "test";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "EnvironmentController > Test");
                return ex.Message;
            }
        }
        
        [HttpGet("{key}")]
        public string GetValue(string key)
        {
            try
            {
                return _env.GetEnvironmentVariable(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "EnvironmentController > GetValue");
                return ex.Message;
            }
        }
    }
}