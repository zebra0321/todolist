using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Models;
using ToDoList.Web.Services;

namespace ToDoList.Web.Controllers.API {
    [ApiController]
    [Route("api/[controller]")]
    public class ToDoListController: ControllerBase 
    {
        private readonly ILogger<ToDoListController> _logger;
        private readonly IApiService _apiService;

        public ToDoListController(ILogger<ToDoListController> logger, IApiService apiService)
        {
            _logger = logger;
            _apiService = apiService;
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create(TodoItem item) 
        {
            try 
            {
                if (item == null)
                {
                    return BadRequest();
                }

                bool isSuccessful = await _apiService.PostToDoItem(item);
                if (isSuccessful) 
                {
                    return Ok();
                }
                else 
                {
                    throw new Exception(isSuccessful.ToString());
                }
            }
            catch (Exception ex) 
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost("post")]
        public async Task<IActionResult> Post(TodoItem item) 
        {
            try 
            {
                if (item == null)
                {
                    return BadRequest();
                }

                bool isSuccessful = await _apiService.PutToDoItem(item);
                if (isSuccessful) 
                {
                    return Ok();
                }
                else 
                {
                    throw new Exception(isSuccessful.ToString());
                }
            }
            catch (Exception ex) 
            {
                return StatusCode(500, ex.Message);
            }
        }
        
        [HttpPost("delete/{id}")]
        public async Task<ActionResult<IEnumerable<TodoItem>>> Delete(int id)
        {
            try
            {
                bool isSuccessful = await _apiService.DeleteToDoItem(id);
                if (isSuccessful)
                {
                    return Ok();
                }
                else 
                {
                    return BadRequest();
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("get/{id}")]
        public async Task<ActionResult<TodoItem>> GetList(int id)
        {
            try
            {
                TodoItem result = await _apiService.GetToDoItem(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet("get")]
        public async Task<ActionResult<IEnumerable<TodoItem>>> GetList()
        {
            try
            {
                List<TodoItem> result = await _apiService.GetToDoItems();
                return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}