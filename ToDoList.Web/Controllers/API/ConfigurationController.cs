using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class ConfigurationController: ControllerBase
    {
        private readonly ICustomConfiguration _config;
        private readonly ILogger<ConfigurationController> _logger;

        public ConfigurationController(ICustomConfiguration config, ILogger<ConfigurationController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [HttpGet]
        public string Test()
        {
            try
            {
                return "test";
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ConfigurationController > Test");
                return ex.Message;
            }
        }
        
        [HttpGet("{key}")]
        public string GetValue(string key)
        {
            try
            {
                return _config.GetValue<string>(key);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ConfigurationController > GetValue");
                return ex.Message;
            }
        }
    }
}