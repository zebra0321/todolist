using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoList.Web.Models;

namespace ToDoList.Web.Services {
    public interface IApiService {
        public Task<List<TodoItem>> GetToDoItems();

        public Task<TodoItem> GetToDoItem(int id);

        public Task<bool> PutToDoItem(TodoItem item);

        public Task<bool> PostToDoItem(TodoItem item);

        public Task<bool> DeleteToDoItem(int id);
    }
}