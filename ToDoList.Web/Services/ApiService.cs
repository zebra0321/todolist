using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ToDoList.Web.Models;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Services {
    public class ApiService: IApiService 
    {
        private readonly IApiRepository _apiRepository;
        private readonly ILogger<ApiService> _logger;
        public ApiService(IApiRepository apiRepository, ILogger<ApiService> logger)
        {
            _apiRepository = apiRepository;
            _logger = logger;
        }

        public async Task<List<TodoItem>> GetToDoItems() {
            try 
            {
                var list = await _apiRepository.GetToDoItems(); 
                return list.Select(item => DtoMapViewModel(item)).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ApiService > GetToDoItems");
                return null;
            }
        }

        public async Task<TodoItem> GetToDoItem(int id) {
            try
            {
                var item = await _apiRepository.GetToDoItem(id);
                return DtoMapViewModel(item);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "ApiService > GetToDoItem");
                return null;
            }
        }

        public async Task<bool> PutToDoItem(TodoItem item) {
            var result = await _apiRepository.PutToDoItem(item);
            return result;
        }

        public async Task<bool> PostToDoItem(TodoItem item) {
            var result = await _apiRepository.PostToDoItem(item);
            return result;
        }

        public async Task<bool> DeleteToDoItem(int id) {
            var result = await _apiRepository.DeleteToDoItem(id);
            return result;
        }

        private TodoItem DtoMapViewModel(ToDoList.Core.Entities.TodoItem item) {
            return new TodoItem 
            {
                Id = item.Id,
                Title = item.Title,
                DueDate = item.DueDate,
                IsComplete = item.IsComplete,
                CreatedAt = item.CreatedAt,
                ModifiedAt = item.ModifiedAt
            };
        }
    }
}