namespace ToDoList.Web.Constants
{
    public class ConfigurationConstants
    {
        public static readonly string ApiBaseUrl = "ApiBaseUrl";
        public static readonly string HttpClientHandler = "httpClientHandler";
    }
}