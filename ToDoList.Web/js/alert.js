class alert {
    static get successPopup() { return '.alert.alert-success'; }
    static get failPopup() { return '.alert.alert-danger'; }
    static get messageSpan() { return 'span.alert-message'; }
    static get popupAnimation() { return 'slow'; }

    static setShowPopup(selector, message) {
        $(selector).children(alert.messageSpan).text(message);
        $(selector).show(alert.popupAnimation);
        alert.setHidePopup(selector);
    }

    static setHidePopup(selector) {
        setTimeout(() => {
            $(selector).hide(alert.popupAnimation);
        }, 3000);
    }
}