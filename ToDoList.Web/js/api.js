class api {
    static get getList() { return '/api/todolist/get' };
    static get getItem() { return '/api/todolist/get/' };
    static get createItem() { return '/api/todolist/create' };
    static get updateItem() { return '/api/todolist/post' };
    static get deleteItem() { return '/api/todolist/delete/' };
}