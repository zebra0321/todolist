class modal {
    static get editModal() { return '#editModal' };
    static get editModalLabel() { return '#editModalLabel' };
    static get editForm() { return '#editForm' };
    static get editInputId() { return '#form-input-id' };
    static get editInputTitle() { return '#form-input-title' };
    static get editInputDueDate() { return '#form-input-dueDate' };
    static get editInputIsComplete() { return '#form-input-isComplete' };
    static get deleteModal() { return '#deleteModal' };
    static get deleteForm() { return '#deleteForm' };
    static get deleteId() { return '#form-delete-id' };
    static get showModal() { return 'show'};
    static get hideModal() { return 'hide'};

    static showDeleteModal(id) {
        var $modal = $(modal.deleteModal);
        var $form = $modal.find(modal.deleteForm);
        $form[0].reset();

        var $idField = $form.find(modal.deleteId);
        $idField.attr('value', id);

        $modal.modal(modal.showModal);
    }

    static hideDeleteModal() {
        $(modal.deleteModal).modal(modal.hideModal);
    }

    static showCreateModal() {
        this.setEditModal(null);
    }

    static showEditModal(id) {
        getItem(id, modal.setEditModal);
    }

    static hideEditModal() {
        $(modal.editModal).modal(modal.hideModal);
    }

    static setEditModal(item) {
        var $modal = $(modal.editModal);
        var $modalTitle = $modal.find(modal.editModalLabel);
        $modalTitle.text('Add To-do Item');

        var $form = $modal.find(modal.editForm);
        var $idField = $form.find(modal.editInputId);
        var $titleField = $form.find(modal.editInputTitle);
        var $dueDateField = $form.find(modal.editInputDueDate);
        var $isCompleteField = $form.find(modal.editInputIsComplete);

        if (item != null) {
            $modalTitle.text('Edit To-do Item');
            console.log(item);
            $idField.val(item.id);
            $titleField.val(item.title);
            
            var dateStr = item.dueDate.substring(0, 10);
            $dueDateField.val(dateStr);
            $isCompleteField.prop('checked', item.isComplete);
        }
        else {
            $idField.val('');
            $titleField.val('');
            $dueDateField.val('');
            $isCompleteField.prop('checked', false);
        }

        $modal.modal(modal.showModal);
    }
}