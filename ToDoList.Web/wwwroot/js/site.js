class alert {
    static get successPopup() { return '.alert.alert-success'; }
    static get failPopup() { return '.alert.alert-danger'; }
    static get messageSpan() { return 'span.alert-message'; }
    static get popupAnimation() { return 'slow'; }

    static setShowPopup(selector, message) {
        $(selector).children(alert.messageSpan).text(message);
        $(selector).show(alert.popupAnimation);
        alert.setHidePopup(selector);
    }

    static setHidePopup(selector) {
        setTimeout(() => {
            $(selector).hide(alert.popupAnimation);
        }, 3000);
    }
}
class api {
    static get getList() { return '/api/todolist/get' };
    static get getItem() { return '/api/todolist/get/' };
    static get createItem() { return '/api/todolist/create' };
    static get updateItem() { return '/api/todolist/post' };
    static get deleteItem() { return '/api/todolist/delete/' };
}
$(document).ready(function(e){
    getItems(true);
    $('#btn-add-item').click(() => {
        modal.showCreateModal();
    });
    $(modal.deleteForm).submit((e) => {
        e.preventDefault();
        modal.hideDeleteModal();
        deleteItem().done(() => {
            getItems(true);
        });
    });
    $(modal.editForm).submit((e) => {
        e.preventDefault();
        modal.hideEditModal();

        if ($(modal.editInputId).val().length > 0) {
            updateItem().done(() => {
                getItems(true);
            });
        }
        else {
            createItem().done(() => {
                getItems(true)
            });
        }
    });
});

function deleteItem() {
    return $.ajax({
        url: api.deleteItem + $(modal.deleteId).val(),
        method: 'POST',
        contentType: 'application/json',
        success: function(data, textStatus, jqXHR) {
            alert.setShowPopup(alert.successPopup, 'Item deleted');
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert.setShowPopup(alert.failPopup, 'Failed to delete item');
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
        }
    });
}

function createItem() {
    return $.ajax({
        url: api.createItem,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            title: $(modal.editInputTitle).val(),
            dueDate: $(modal.editInputDueDate).val(),
            isComplete: $(modal.editInputIsComplete).prop('checked'),
        }),
        success: function(data, textStatus, jqXHR) {
            alert.setShowPopup(alert.successPopup, 'Item created');
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert.setShowPopup(alert.failPopup, 'Failed to create item');
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
        }
    });
}

function updateItem() {
    return $.ajax({
        url: api.updateItem,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            id: $(modal.editInputId).val(),
            title: $(modal.editInputTitle).val(),
            dueDate: $(modal.editInputDueDate).val(),
            isComplete: $(modal.editInputIsComplete).prop('checked'),
        }),
        success: function(data, textStatus, jqXHR) {
            alert.setShowPopup(alert.successPopup, 'Item updated');
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert.setShowPopup(alert.failPopup, 'Failed to update item');
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
        }
    });
}

function updateItemStatus(e) {
    return $.ajax({
        url: api.updateItem,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify({
            id: e.value,
            isComplete: e.checked ? true : false
        }),
        success: function(data, textStatus, jqXHR) {
            alert.setShowPopup(alert.successPopup, 'Status updated');
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert.setShowPopup(alert.failPopup, 'Failed to update status');
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
        }
    })
}

function getItem(id, callback) {
    $.ajax({
        url: api.getItem + id,
        method: 'GET',
        contentType: 'application/json',
        success: function(data, textStatus, jqXHR) {
            if (callback) {
                callback(data);
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            alert.setShowPopup(alert.failPopup, 'Failed to get item');
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
            return null;
        }
    })
}

function getItems(initList) {
    return $.ajax({
        url: api.getList,
        method: 'GET',
        success: function(data, textStatus, jqXHR) {
            var cnt = data.length;
            if (cnt > 0) {
                var incompleteCnt = data.filter(item => !item.isComplete).length;
                $('.list-cnt').text(`Pending items: ${incompleteCnt}`);
            }
            else {
                $('.list-cnt').text('The list is empty. Start filling the list by adding an item.');
            }

            if (initList)
            {
                var $template = $('#list-group-item-0');
                $template.siblings().remove();

                for (var i = 1; i <= cnt; i++) 
                {
                    var $newItem = $template.clone();
                    var itemData = data[i - 1];

                    $newItem.attr('id', `list-group-item-${i}`);
                    $newItem.attr('for', `item_checkbox_${i}`);
                    $newItem.removeClass('d-none');

                    var $newItemCheckbox = $newItem.children('#item_checkbox_0');
                    $newItemCheckbox.attr('id', `item_checkbox_${i}`);
                    $newItemCheckbox.attr('value', itemData.id);
                    $newItemCheckbox.on('click', (e) => {
                        updateItemStatus(e.target).done(getItems);
                    });

                    if (itemData.isComplete) {
                        $newItemCheckbox.attr('checked', 'checked');
                    }
                    
                    var $newItemTitle = $newItem.find('#item_title_0');
                    $newItemTitle.attr('id', `item_title_${i}`);
                    $newItemTitle.text(itemData.title);

                    var $newItemDueDate = $newItem.find('#item_duedate_0');
                    $newItemDueDate.attr('id', `item_duedate_${i}`);

                    var dueDate = itemData.dueDate.substring(0, 10);
                    $newItemDueDate.text(dueDate);

                    var $newItemDropdownMenu = $newItem.find('#dropdown_menu_0');
                    $newItemDropdownMenu.attr('id', `dropdown_menu_${i}`);

                    var $newItemDropdownMenuBtn = $newItemDropdownMenu.find('#dropdown_menu_link_0');
                    $newItemDropdownMenuBtn.attr('id', `dropdown_menu_link_${i}`);

                    var $newItemDropdownEditBtn = $newItemDropdownMenu.find('.dropdown-item.dropdown-item-edit');
                    $newItemDropdownEditBtn.attr('data-item-id', itemData.id);

                    var $newItemDropdownDeleteBtn = $newItemDropdownMenu.find('.dropdown-item.dropdown-item-delete');
                    $newItemDropdownDeleteBtn.attr('data-item-id', itemData.id);

                    $newItem.insertAfter($template);
                }

                $('.dropdown-item-edit').on('click', (e) => {
                    modal.showEditModal($(e.target).attr('data-item-id'));
                });
                $('.dropdown-item-delete').on('click', (e) => {
                    modal.showDeleteModal($(e.target).attr('data-item-id'));
                });
            }
        },
        error: function(jqXHR, textStatus, errorThrown){
            console.log('jqXHR:', jqXHR);
            console.log('text status:', textStatus);
            console.log('error thrown:', errorThrown);
        }
    })
}
class modal {
    static get editModal() { return '#editModal' };
    static get editModalLabel() { return '#editModalLabel' };
    static get editForm() { return '#editForm' };
    static get editInputId() { return '#form-input-id' };
    static get editInputTitle() { return '#form-input-title' };
    static get editInputDueDate() { return '#form-input-dueDate' };
    static get editInputIsComplete() { return '#form-input-isComplete' };
    static get deleteModal() { return '#deleteModal' };
    static get deleteForm() { return '#deleteForm' };
    static get deleteId() { return '#form-delete-id' };
    static get showModal() { return 'show'};
    static get hideModal() { return 'hide'};

    static showDeleteModal(id) {
        var $modal = $(modal.deleteModal);
        var $form = $modal.find(modal.deleteForm);
        $form[0].reset();

        var $idField = $form.find(modal.deleteId);
        $idField.attr('value', id);

        $modal.modal(modal.showModal);
    }

    static hideDeleteModal() {
        $(modal.deleteModal).modal(modal.hideModal);
    }

    static showCreateModal() {
        this.setEditModal(null);
    }

    static showEditModal(id) {
        getItem(id, modal.setEditModal);
    }

    static hideEditModal() {
        $(modal.editModal).modal(modal.hideModal);
    }

    static setEditModal(item) {
        var $modal = $(modal.editModal);
        var $modalTitle = $modal.find(modal.editModalLabel);
        $modalTitle.text('Add To-do Item');

        var $form = $modal.find(modal.editForm);
        var $idField = $form.find(modal.editInputId);
        var $titleField = $form.find(modal.editInputTitle);
        var $dueDateField = $form.find(modal.editInputDueDate);
        var $isCompleteField = $form.find(modal.editInputIsComplete);

        if (item != null) {
            $modalTitle.text('Edit To-do Item');
            console.log(item);
            $idField.val(item.id);
            $titleField.val(item.title);
            
            var dateStr = item.dueDate.substring(0, 10);
            $dueDateField.val(dateStr);
            $isCompleteField.prop('checked', item.isComplete);
        }
        else {
            $idField.val('');
            $titleField.val('');
            $dueDateField.val('');
            $isCompleteField.prop('checked', false);
        }

        $modal.modal(modal.showModal);
    }
}