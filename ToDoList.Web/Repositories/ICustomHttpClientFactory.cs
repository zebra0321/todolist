using System.Net.Http;
using ToDoList.Web.Constants;

namespace ToDoList.Web.Repositories
{
    public interface ICustomHttpClientFactory
    {
        public HttpClient CreateClient();
    }
}