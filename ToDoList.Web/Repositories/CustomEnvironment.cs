using System;
using System.Diagnostics.CodeAnalysis;

namespace ToDoList.Web.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomEnvironment: ICustomEnvironment
    {
        public CustomEnvironment()
        {

        }

        public string GetEnvironmentVariable(string key)
        {
            return Environment.GetEnvironmentVariable(key);
        }
    }
}