using System.Collections.Generic;
using System.Threading.Tasks;
using ToDoList.Core.Entities;

namespace ToDoList.Web.Repositories {
    public interface IApiRepository {
        public Task<List<TodoItem>> GetToDoItems();

        public Task<TodoItem> GetToDoItem(int id);

        public Task<bool> PutToDoItem(Models.TodoItem item);

        public Task<bool> PostToDoItem(Models.TodoItem item);

        public Task<bool> DeleteToDoItem(int id);
    }
}