using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using ToDoList.Web.Constants;

namespace ToDoList.Web.Repositories
{
    [ExcludeFromCodeCoverage]
    public class CustomHttpClientFactory: ICustomHttpClientFactory
    {
        private readonly IHttpClientFactory _factory;
        public CustomHttpClientFactory(IHttpClientFactory factory)
        {
            _factory = factory;
        }

        public HttpClient CreateClient()
        {
            return _factory.CreateClient(ConfigurationConstants.HttpClientHandler);
        }
    }
}