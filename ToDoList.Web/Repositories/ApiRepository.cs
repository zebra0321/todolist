using System;
using System.Text;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.Json;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ToDoList.Core.Entities;
using ToDoList.Web.Constants;
using ToDoList.Web.Repositories;

namespace ToDoList.Web.Repositories {
    public class ApiRepository: IApiRepository {
        private readonly ICustomHttpClientFactory _clientFactory;
        private readonly ICustomEnvironment _environment;
        private readonly ICustomConfiguration _configuration;
        private readonly ILogger _logger;
        private string _apiBaseUrl;
        public ApiRepository(ICustomHttpClientFactory clientFactory, ICustomEnvironment environment, ICustomConfiguration configuration, ILogger<ApiRepository> logger) {
            _clientFactory = clientFactory;
            _environment = environment;
            _configuration = configuration;
            _logger = logger;
            _apiBaseUrl = _configuration.GetValue<string>(ConfigurationConstants.ApiBaseUrl); //_environment.GetEnvironmentVariable(ConfigurationConstants.ApiBaseUrl);
        }

        public async Task<List<TodoItem>> GetToDoItems() {
            List<TodoItem> result = null;

            try { 
                var httpClient = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Get, _apiBaseUrl);
                var response = await httpClient.SendAsync(request);
                
                if (response.IsSuccessStatusCode) {
                    using (var responseStream = await response.Content.ReadAsStreamAsync()) {
                        result = await JsonSerializer.DeserializeAsync<List<TodoItem>>(responseStream);
                    }
                }
            }  
            catch (Exception ex) {
                _logger.LogError(ex, "ApiRepository > GetToDoItems Error: {message} - {line}", ex.Message, ex.StackTrace);
                _logger.LogError($"Api: {_apiBaseUrl}");
                _logger.LogError($"From Environment: {_environment.GetEnvironmentVariable(ConfigurationConstants.ApiBaseUrl)}");
            }          

            return result;
        }

        public async Task<TodoItem> GetToDoItem(int id) {
            TodoItem result = null;

            try { 
                var httpClient = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Get, _apiBaseUrl + $"{id}");
                var response = await httpClient.SendAsync(request);
                
                if (response.IsSuccessStatusCode) {
                    using (var responseStream = await response.Content.ReadAsStreamAsync()) {
                        result = await JsonSerializer.DeserializeAsync<TodoItem>(responseStream);
                    }
                }
            }  
            catch (Exception ex) {
                _logger.LogError(ex, "ApiRepository > GetToDoItems Error: {message} - {line}", ex.Message, ex.StackTrace);
            }          

            return result;
        }

        public async Task<bool> PutToDoItem(Models.TodoItem item) {
            try { 
                var httpClient = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Put, _apiBaseUrl + $"{item.Id}");
                request.Content = new StringContent(JsonSerializer.Serialize(item), Encoding.UTF8, "application/json");

                var response = await httpClient.SendAsync(request);
                
                if (response.IsSuccessStatusCode) {
                    return true;
                }
            }  
            catch (Exception ex) {
                _logger.LogError(ex, "ApiRepository > PutToDoItem Error: {message} - {line}", ex.Message, ex.StackTrace);
            }          

            return false;
        }

        public async Task<bool> PostToDoItem(Models.TodoItem item) {
            try { 
                var httpClient = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Post, _apiBaseUrl);
                request.Content = new StringContent(JsonSerializer.Serialize(item), Encoding.UTF8, "application/json");

                var response = await httpClient.SendAsync(request);
                
                if (response.IsSuccessStatusCode) {
                    return true;
                }
            }  
            catch (Exception ex) {
                _logger.LogError(ex, "ApiRepository > PostToDoItem Error: {message} - {line}", ex.Message, ex.StackTrace);
            }          

            return false;
        }

        public async Task<bool> DeleteToDoItem(int id) {
            try { 
                var httpClient = _clientFactory.CreateClient();
                var request = new HttpRequestMessage(HttpMethod.Delete, _apiBaseUrl + $"{id}");
                var response = await httpClient.SendAsync(request);
                
                if (response.IsSuccessStatusCode) {
                    return true;
                }
            }  
            catch (Exception ex) {
                _logger.LogError(ex, "ApiRepository > DeleteToDoItem Error: {message} - {line}", ex.Message, ex.StackTrace);
            }          

            return false;
        }
    }
}