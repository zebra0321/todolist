using System;

namespace ToDoList.Web.Repositories
{
    public interface ICustomEnvironment
    {
        public string GetEnvironmentVariable(string key);
    }
}