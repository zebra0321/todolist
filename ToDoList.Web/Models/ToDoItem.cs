using System;
using System.Text.Json.Serialization;

namespace ToDoList.Web.Models {
    public class TodoItem {
        [JsonPropertyName("id")]
        public int Id { get; set; }
        [JsonPropertyName("title")]
        public string Title { get; set; }
        [JsonPropertyName("dueDate")]
        public DateTime? DueDate { get; set; }
        [JsonPropertyName("isComplete")]
        public bool? IsComplete { get; set; }
        [JsonPropertyName("createdAt")]
        public DateTime CreatedAt { get; set; }
        [JsonPropertyName("modifiedAt")]
        public DateTime? ModifiedAt { get; set; }
    }
}