using System;

namespace ToDoList.Web.ViewModels.Components.ToDoList 
{
    public class ToDoItemViewModel {
        public int Id { get; set; }
        public DateTime? DueDate { get; set; }
        public string Title { get; set; }
        public bool IsComplete { get; set; }
    }
} 