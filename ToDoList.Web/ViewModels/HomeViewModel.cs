using ToDoList.Web.ViewModels.Components.ToDoList;
using System.Collections.Generic;

namespace ToDoList.Web.ViewModels 
{
    public class HomeViewModel {
        public List<ToDoItemViewModel> listItems;
    }
}